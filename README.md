# Instructions
- Add the jsons to the data folder
- Add names of boards to `board_groups.json`
- Add names of jsons to `export.py`
- Run `export.py`
- The output will be in `output/all.csv`. Use conditional formatting.
