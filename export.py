import json
import dateutil.parser
from src import utils
from src import parser
from src import gantt
import csv

style = {
    "Unplanned": "u",
    "Planned": "p",
    "Inactive": "i",
    "Active": "a"
}

fnames = ["science", "software", "electrical", "chassis", "arm"]

with open('data/board_groups.json', 'r') as f:
    board_groups = json.load(f)

cards = []
fsizes = []
for fname in fnames:
    with open(f'data/{fname}.json', 'r') as f:
        dat = json.load(f)

    # First, retrieve the boards
    fcards = parser.parse_data(dat, board_groups)
    fsizes.append(len(fcards))
    cards.extend(fcards)

gt = gantt.generate_gantt(cards, style=style)
# Insert teams
for i, row in enumerate(gt):
    # Get team
    l = i
    for j, n in enumerate(fsizes):
        if n <= l:
            l -= n
        else:
            team = fnames[j]
            break
    row.insert(0, team)
with open(f"output/all.csv", "w") as f:
    writer = csv.writer(f)
    for row in gt:
        writer.writerow(row)

