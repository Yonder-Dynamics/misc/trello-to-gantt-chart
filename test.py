import json
import dateutil.parser
from src import utils
from src import parser
from src import gantt
import csv

fname = "science"
fname = "software"
fname = "electrical"
fname = "chassis"
fname = "arm"

with open(f'data/{fname}.json', 'r') as f:
    dat = json.load(f)

with open('data/board_groups.json', 'r') as f:
    board_groups = json.load(f)

with open('data/blacklist.txt', 'r') as f:
    blacklist = [k.strip() for k in f.readlines()]

"""
for key in dat:
    if key in blacklist:
        continue
    print(key)
    if type(dat[key]) == dict:
        print(dat[key].keys())
    if type(dat[key]) == list:
        print("Array")
        print(dat[key][0].keys())
    else:
        print(dat[key])
#  """

# First, retrieve the boards
cards = parser.parse_data(dat, board_groups)
#  for card in cards:
#      utils.pretty(card)

style = {
    "Unplanned": "u",
    "Planned": "p",
    "Inactive": "i",
    "Active": "a"
}
start = dateutil.parser.parse("2019-09-27 02:39:43.042000+00:00")
end = dateutil.parser.parse("2020-02-24 00:54:22.951000+00:00")
gt = gantt.generate_gantt(cards, style=style, start=start, end=end)
#  gt = gantt.generate_gantt(cards, style=style)
with open(f"output/{fname}.csv", "w") as f:
    writer = csv.writer(f)
    for row in gt:
        writer.writerow(row)
