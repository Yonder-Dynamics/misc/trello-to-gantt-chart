
def pretty(d, indent=0):
   for key, value in d.items():
      print('\t' * indent + str(key))
      if isinstance(value, dict):
         pretty(value, indent+1)
      else:
         print('\t' * (indent+1) + str(value))

def filter_dict(d, keys):
    out = {}
    for k in keys:
        out[k] = d[k]
    return out
