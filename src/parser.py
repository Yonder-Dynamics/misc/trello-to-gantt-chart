from src import utils

def parse_data(dat, board_groups):
    """
    Example output
    name
            Find sample data for growth curves of extremophilic cyanobacteria. We need standards to compare our data too.
    id
            5dc312272333248eaf2bbe4d
    idList
            5d90fb88ed72eb1698bc0c73
    idBoard
            5d90faf18c6e520b54cff3ae
    idMembers
            ['5db5e00b1ee6202f518144c7']
    pos
            327679
    dateLastActivity
            2019-11-06T18:38:04.439Z
    desc

    actions
            [{'date': '2019-11-06T18:38:04.465Z', 'user': 'Luke Piszkin'},
             {'date': '2019-11-06T18:38:04.426Z', 'user': 'Luke Piszkin'},
             {'date': '2019-11-06T18:34:15.236Z', 'user': 'Luke Piszkin', 'action': 'creation'}]
    """
    boards_raw = dat["lists"]
    boards = []
    for b in boards_raw:
        boards.append(utils.filter_dict(b, ["name", "id", "pos"]))

    print(boards)
    inactive_boards = [board for board in boards if board["name"] in board_groups["inactive"]]
    active_boards = [board for board in boards if board["name"] in board_groups["active"]]
    print("Active boards")
    print([board["name"] for board in active_boards])
    print("Inactive boards")
    print([board["name"] for board in inactive_boards])

    def id_to_state(board_id):
        for board in inactive_boards:
            if board["id"] == board_id:
                return "Inactive"
        return "Active"

    # checklists = those checklists within a task
    # lists = boards
    # actions = list of actions performed on tasks
    # cards = tasks

    # Next, retrieve all tasks
    cards_raw = dat["cards"]
    cards = []
    for c in cards_raw:
        d = utils.filter_dict(c, [
            "name", "id", "idList", "idBoard", "idMembers", "pos",
            "dateLastActivity", "desc"
        ])
        d["actions"] = []
        cards.append(d)

    def retrieve_card(cid):
        for card in cards:
            if card["id"] == cid:
                return card
        return None

    actions_raw = dat["actions"]
    # types we might care about:
    # updateCard, createCard
    for a in actions_raw:
        t = a["type"]
        if t not in ["updateCard", "createCard"]:
            continue
        card = retrieve_card(a["data"]["card"]["id"])
        action = {
            "date": a["date"],
            "user": a["memberCreator"]["fullName"],
        }
        if t == "updateCard":
            # Add action to card
            # Narrow types of boards to active or inactive to standarize
            #  utils.pretty(a)
            change = list(a["data"]["old"].keys())[0]
            if change == "idList":
                bid = a["data"]["card"][change]
                state = id_to_state(bid)
                action["action"] = state
            else:
                continue
        elif t == "createCard":
            # Update cards with creation
            action["action"] = "Planned"
        card["actions"].append(action)
    return cards
