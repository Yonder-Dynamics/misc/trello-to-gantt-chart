import dateutil.parser
import datetime

def generate_gantt(cards, day_res=7, style=None, start=None, end=None):
    # Get date range
    if start is None:
        dates = []
        states = set()
        for card in cards:
            date = dateutil.parser.parse(card["dateLastActivity"])
            for action in card["actions"]:
                date = dateutil.parser.parse(action["date"])
                dates.append(date)
                states.add(action["action"])
        dates.sort()
        end = dates[-1]
        start = dates[0]
        print("Start, end")
        print(dates[0], dates[-1])
    days = (end - start).days

    # Discretize the date range
    num_cells = int(days/day_res+0.5)
    rows = []
    for card in cards:
        cells = [0 for i in range(num_cells)]

        # Sort to deal with states in order
        def get_key(action):
            return dateutil.parser.parse(action["date"])
        card["actions"].sort(key=get_key)

        # Set until last state
        last_state = "Unplanned"
        last_state_i = 0
        for i, action in enumerate(card["actions"]):
            # reverse search
            ds = (get_key(action) - start).days
            ind = ds // day_res
            for j in range(last_state_i, ind):
                if style is None:
                    cells[j] = last_state
                else:
                    cells[j] = style[last_state]
            last_state_i = ind
            last_state = action["action"]

        for j in range(last_state_i, num_cells):
            if style is None:
                cells[j] = last_state
            else:
                cells[j] = style[last_state]

        rows.append([card["name"].title()] + cells)
    return rows
